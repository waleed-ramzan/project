<?php

namespace App\Http\Controllers;
use App\models\student_course;
use App\Models\Student;
use App\models\course;
use Illuminate\Http\Request;


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = course::all() ;
        $students=student::all();
        return view('course',['courses'=>$courses,'layout'=>'index']);
        return view('course',['students'=>$students,'layout'=>'index']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $courses = course::all() ;
      $students = Student::all() ;

      return view('course',['courses'=>$courses,'students'=>$students,'layout'=>'create']);
      //return view('course',['students'=>$students,'layout'=>'create']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = new course() ;
        $course->id = $request->input('id');
        $course->name = $request->input('name');
        $course->save() ;
        $course =course::find(2);
        $course->students()->attach(2);
        return redirect('/course') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = course::find($id);
        $courses =course::all() ;
        return view('course',['courses'=>$courses,'course'=>$course,'layout'=>'show']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $course = course::find($id);
      $courses = course::all() ;
      return view('course',['courses'=>$courses,'course'=>$course,'layout'=>'edit']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = course::find($id);
        $course->id = $request->input('id');
        $course->name = $request->input('name');
        $course->save() ;
        return redirect('/course');
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $course = course::find($id);
      $course->delete() ;
      return redirect('/course') ;
    }
}
