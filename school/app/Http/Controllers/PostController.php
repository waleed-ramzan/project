<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Student;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $students = Student::all();
        $posts = Post::where('student_id',auth()->user()->student->id)->get();
        return view('posts.index', ['posts'=>$posts,'students'=>$students],compact('posts','students'));


    }

    public function create()
    {
        $students = Student::all();
        $posts = Post::all();
        return view('posts.create', compact('posts','students'));


    }

    public function store(Post $post, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            ]);
        $post = new Post();

        $post->title = $request->title;
        $post->body = $request->body;
        $post->student_id = $request->student_id;
        $post->published_at = $request->published_at;

        $post->save();
        return redirect('/home')->with('success','Post created successfully!');
    }

    public function show(Post $post,Request $request,$id)
    {
        $post= Post::find($id);

        return view('posts.show', compact('post'));
    }

    public function edit($id)
    {   $post= Post::find($id);
        return view('posts.edit', compact('post'));
    }

    public function update( Request $request,$id)
    {



        $request->validate([
            'title' => 'required',
            'body' => 'required',
            ]);
            $post=Post::find($id);
//$data= $request->all();
        // $post->title = $request->title;
        // $post->body = $request->body;
        // $post->published_at = $request->published_at;

        $post->update($request->except('method'));
        return redirect('/home')->with('success','Post updated successfully!');
    }

    public function destroy(Post $post,$id)
    {
        $post=Post::find($id);
        $post->delete();
        return redirect('/home')->with('success','Post deleted successfully!');
    }
}
