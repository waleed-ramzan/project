<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TeacherAdded
{
    use Dispatchable, SerializesModels;

    public $teacher;

    public $student;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($teacher, $student)
    {
        $this->teacher = $teacher;
        $this->student = $student;
    }
}
