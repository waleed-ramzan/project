<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Events\PostCreated;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(Request $request)
    {


        $post = Post::create($request->all());
        event(new PostCreated($post)); // dispatch event from here

        //You can use the below commented code
        //PostCreated::dispatch($post);

        return redirect()->route('posts.index')->with('success','Post created successfully.');
    }
}
