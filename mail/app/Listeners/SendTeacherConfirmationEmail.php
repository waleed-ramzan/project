<?php

namespace App\Listeners;

use App\Events\TeacherAdded;
use App\Mail\TeacherConfirmationEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendTeacherConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherAdded  $event
     * @return void
     */
    public function handle(TeacherAdded $event)
    {
        Mail::to($event->teacher->email)->send(
            new TeacherConfirmationEmail($event->teacher, $event->student)
        );
    }
}
