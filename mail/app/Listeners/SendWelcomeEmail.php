<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Events\UserRegistered;
class SendWelcomeEmail
{

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $data = array('name' => $event->user->name, 'email' => $event->user->email, 'body' => 'Welcome to our website. Hope you will enjoy our articles');

        Mail::send('emails.mail', $data, function($message) use ($data) {
            $message->to($data['email'])
                    ->subject('Welcome to our Website');
            $message->from('waleedramzan301@gmail.com');
        });
    }
}
