<?php

use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\EmailController;
use App\Mail\MailtrapExample;

Route::get('mail/send', [MailController::class, 'send']);

