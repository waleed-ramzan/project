<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
class EmailController extends Controller
{
    public function create()
    {

        return view('email');
    }

    public function sendEmail(Request $request)
    {
        Mail::to('waleedramzan301@gmail.com')->send(new OrderShipped());


        return back()->with(['message' => 'Email successfully sent!']);
    }
}
