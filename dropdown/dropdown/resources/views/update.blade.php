<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="content">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dropdown Country, States, City</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <h2 class="mb-4">Country State City Dropdown</h2>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">Update Country, State, City</h5>
                        <form action="{{ url('/update/'.$worlds->id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Country</label>
                                <input value="{{ $worlds->country }}" name="country" type="text" class="form-control"  placeholder="Enter the first name">
                                @error('country')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <input value="{{ $worlds->state }}" name="state" type="text" class="form-control"  placeholder="Enter second name">
                                @error('state')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>City</label>
                                <input value="{{ $worlds->city }}" name="city" type="text" class="form-control"  placeholder="Enter the Age">
                                @error('city')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <input type="submit" class="btn btn-info" value="Update">
                            <input type="reset" class="btn btn-warning" value="Reset">
                        </form>
                    </div>
                </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
