<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="content">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dropdown Country, States, City</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <h2 class="mb-4">Country State City Dropdown</h2>
                <form action="{{route('dependent-dropdown')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <select  id ="country" name="country" class="form-control">
                            <option value="">Select Country</option>
                            @foreach ($countries as $country)
                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                        @endforeach
                        </select>
                        @error('country')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <select id ="state" name="state" class="form-control">
                            <option selected value="">Select State</option>
                        </select>
                        @error('state')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select id ="city" name="city" class="form-control">
                            <option selected value="">Select City</option>
                        </select>
                        @error('city')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="m-1 p-1">
                    <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-body">
            <h5 class="card-title">List of Country, State and City</h5>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Country</th>
                    <th scope="col">State</th>
                    <th scope="col">City</th>
                    <th scope="col">Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($worlds as $world)
                    <tr>
                        <td>{{ $world->country }}</td>
                        <td>{{ $world->state }}</td>
                        <td>{{ $world->city }}</td>
                        <td>
                            <a href="{{ url('/edit/'.$world->id) }}" class="btn btn-sm btn-warning">Edit</a>
                            <a href="{{ url('/delete/'.$world->id) }}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
        $('#country').on('change', function() {
        var country = this.value;
        $("#state").html('');
        $.ajax({
        url:"{{url('get-states-by-country')}}",
        type: "POST",
        data: {
        country: country,
        _token: '{{csrf_token()}}'
        },
        dataType : 'json',
        success: function(result){
        $('#state').html('<option value="">Select State First</option>');
        $.each(result.states,function(value,value){
        $("#state").append('<option value="'+value.name+'">'+value.name+'</option>');
        });
        }
        });
        });
        $('#state').on('change', function() {
        var state = this.value;
        $("#city").html('');
        $.ajax({
        url:"{{url('get-cities-by-state')}}",
        type: "POST",
        data: {
        state: state,
        _token: '{{csrf_token()}}'
        },
        dataType : 'json',
        success: function(result){
        $('#city').html('<option value="">Select City First</option>');

        $.each(result.cities,function(value,value){
        $("#city").append('<option value="'+value.name+'">'+value.name+'</option>');
        });
        }
        });
        });
        });
        </script>
</body>
</html>
