<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use Illuminate\Http\Request;
use App\Models\{Country, State, City, Worlds};

class DropdownController extends Controller
{
    public function index(Request $request)
    {
            $worlds = Worlds::all();
            $countries =  Country::all();

            return view ('welcome' ,compact('countries','worlds'));

    }
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
  {

    $company = new Worlds;

    $company->country = $request->country;
    $company->state = $request->state;
    $company->city = $request->city;

    $company->save();
    //  dd($company);
    return redirect('/dependent-dropdown');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { $worlds = Worlds::find($id);
        return view('update',compact('worlds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserStoreRequest $request, $id)
    {
        $worlds = Worlds::find($id);
      $worlds->country = $request->input('country') ;
      $worlds->state = $request->input('state') ;
      $worlds->city = $request->input('city') ;
      $worlds->save();
      return redirect('/dependent-dropdown') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getState(Request $request)
    {
        $data['states'] = State::where("country",$request->country)
                    ->get(["name","id"]);
        return response()->json($data);
    }
    public function getCity(Request $request)
    {
        $data['cities'] = City::where("state",$request->state)
                    ->get(["name","id"]);
        return response()->json($data);
    }
    public function destroy($id)
    {
        $worlds=Worlds::find($id);
         $worlds->delete();
       return  redirect('/dependent-dropdown');
    }

    // public function fetchState(Request $request)
    // {
    //     $data['states'] = State::where("country_id",$request->country_id)->get(["name", "id"]);
    //     return response()->json($data);
    // }

    // public function fetchCity(Request $request)
    // {
    //     $data['cities'] = City::where("state_id",$request->state_id)->get(["name", "id"]);
    //     return response()->json($data);
    // }
}
