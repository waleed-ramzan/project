<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Worlds extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'country',
        'state',
        'city'
    ];
    public function states()
    {
        $this->belongsTo(State::class,'state','id');
    }
    public function cities()
    {
        $this->belongsTo(City::class,'city','id');
    }
    public function country()
    {
        $this->belongsTo(Country::class,'country','id');
    }
}
