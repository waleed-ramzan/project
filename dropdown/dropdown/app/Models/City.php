<?php

namespace App\Models;
use App\Models\State;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public function states()
    {
        $this->belongsTo(State::class,'state','name');
    }
    public function worlds()
    {
        $this->hasMany(Worlds::class);
    }
}
