<?php

namespace App\Models;
use App\Models\Country;
use App\Models\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    public function country()
    {
        $this->belongsTo(Country::class,'country','name');
    }
    public function cities()
    {
        $this->hasMany(City::class);
    }
    public function worlds()
    {
        $this->hasMany(Worlds::class);
    }
    /**
     * Get all of the comments for the State
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function comments(): HasMany
    // {
    //     return $this->hasMany(Comment::class, 'foreign_key', 'local_key');
    // }
}
