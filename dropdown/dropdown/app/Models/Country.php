<?php

namespace App\Models;
use App\Models\State;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $fillable =
    [
'name',
    ];



    public function states()
    {
        $this->hasMany(State::class);
    }
    public function worlds()
    {
        $this->hasMany(Worlds::class);
    }

}
