<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DropdownController;
use App\Http\Controllers\CountryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/


Route::get('dependent-dropdown', [DropdownController::class, 'index'])->name('dependent-dropdown.index');
Route::get('edit/{id}', [DropdownController::class, 'edit']);
Route::get('delete/{id}', [DropdownController::class, 'destroy']);
Route::post('update/{id}', [DropdownController::class, 'update']);
Route::post('dependent-dropdown', [DropdownController::class, 'store'])->name('dependent-dropdown');
Route::post('get-states-by-country', [DropdownController::class, 'getState']);
Route::post('get-cities-by-state', [DropdownController::class, 'getCity']);

// Route::post('dependent-dropdown', [DropdownController::class, ''])->name('dependent-dropdown');
// Route::get('country/{country}/states', [CountryController::class, 'getStates']);
// Route::post('api/fetch-states', [DropdownController::class, 'fetchState']);
// Route::post('api/fetch-cities', [DropdownController::class, 'fetchCity']);
