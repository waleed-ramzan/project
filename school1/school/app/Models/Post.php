<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;


    protected $fillable = [
        'id',
        'title',
         'body',


    ];
    public function students()
    {
        return $this->hasMany(Student::class,'students');
    }
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
