<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('edit/{id}', [TodoController::class, 'edit'])->name('edit');

Route::get('/create', [TodoController::class, 'create'])->name('create.todo');

Route::get('/', [TodoController::class, 'index'])->name('index.todo');

Route::post('/store', [TodoController::class, 'store'])->name('store.todo');


Route::post('/update/{id}', [TodoController::class, 'update'])->name('update.todo');

Route::get('/delete/{id}', [TodoController::class, 'destroy'])->name('delete');

Route::get('/complete/{id}', [TodoController::class, 'complete'])->name('complete');

Route::get('/notcomplete/{id}', [TodoController::class, 'notcomplete'])->name('notcomplete');